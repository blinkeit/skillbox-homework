﻿using System;

namespace Task5
{
    internal class Task5
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter max range number:");
            Random r = new Random();
            int number = r.Next(0, int.Parse(Console.ReadLine()) + 1);

            while (true)
            {
                Console.WriteLine("Enter your number:");
                string userInput = Console.ReadLine();
                if (int.TryParse(userInput, out int userNumber))
                {
                    if (userNumber > number)
                    {
                        Console.WriteLine("Entered number is greater");
                    }
                    else if (userNumber < number)
                    {
                        Console.WriteLine("Entered number is smaller");
                    }
                    else if (userNumber == number)
                    {
                        Console.WriteLine("You guessed the number");
                        Console.ReadKey();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine($"Target number was {number}");
                    Console.ReadKey();
                    break;
                }
            }
        }
    }
}
