﻿using System;

namespace Task2
{
    internal class Task2
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi. How much cards do you have?");
            int amount = int.Parse(Console.ReadLine());
            int sum = 0;
            for (int i = 1; i <= amount; )
            {
                Console.WriteLine($"Enter {i} card value (numbers or first letters):");
                string value = Console.ReadLine();
                if (int.TryParse(value, out int temp))
                {
                    if (temp >= 4 && temp <= 10)
                    {
                        sum += temp;
                        i++;
                    }
                    else
                    {
                        Console.WriteLine("There is no such card in a deck. Sum will be incorrect.");
                    }
                }
                else
                {
                    value = value.ToUpper();
                    switch (value)
                    {
                        case "J":
                            sum += 10;
                            i++;
                            break;
                        case "Q":
                            sum += 10;
                            i++;
                            break;
                        case "K":
                            sum += 10;
                            i++;
                            break;
                        case "A":
                            sum += 10;
                            i++;
                            break;
                        default:
                            Console.WriteLine("There is not such card in a deck or you have entered wrong first letter of card name.");
                            break;
                    }
                }
            }
            Console.WriteLine($"Cards sum is {sum}");
            Console.ReadKey();
        }
    }
}
