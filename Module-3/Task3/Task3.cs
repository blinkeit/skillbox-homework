﻿using System;

namespace Task3
{
    internal class Task3
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter integer (digits only):");
            int number = int.Parse(Console.ReadLine());
            bool isPrime = true;
            int i = 2;
            while (number % i != 0)
            {
                i++;
            }
            if(number % i == 0 && number != i)
            {
                isPrime = false;
            }
            Console.WriteLine(isPrime ? $"Number {number} is prime" : $"Number {number} isn't prime");
            Console.ReadKey();
        }
    }
}
