﻿using System;

namespace Task4
{
    internal class Task4
    {
        static void Main(string[] args)
        {
            int t = int.MaxValue;
            Console.WriteLine("Enter sequence length:");
            int length = int.Parse(Console.ReadLine());
            int num = 0;
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter next number:");
                num = int.Parse(Console.ReadLine());
                if(num < t)
                {
                    t = num;
                }
            }
            Console.WriteLine($"The number {t} is the least sequence element");
            Console.ReadKey();
        }
    }
}
