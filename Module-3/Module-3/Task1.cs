﻿using System;

namespace Module_3
{
    internal class Task1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter integer (digits only):");
            int number = int.Parse(Console.ReadLine());

            if(number % 2 == 0)
            {
                Console.WriteLine($"The number {number} is even");
            }
            else
            {
                Console.WriteLine($"The number {number} is odd");
            }
            Console.ReadKey();
        }
    }
}
