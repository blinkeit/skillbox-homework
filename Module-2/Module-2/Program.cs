﻿using System;

namespace Module_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string fullName = "Allister Crowley";
            int age = 5;
            string email = "a.crowley@gmail.com";
            decimal programmingDegree = 10;
            decimal physicsDegree = 7;
            decimal mathDegree = 7;

            decimal degreeSum;
            decimal averageDegree;

            Console.WriteLine($"{"Full Name:",-20} {fullName}");
            Console.WriteLine($"{"Age:",-20} {age}");
            Console.WriteLine($"{"E-mail:",-20} {email}");
            Console.WriteLine($"{"Programming Degree:",-20} {programmingDegree}");
            Console.WriteLine($"{"Physics Degree:",-20} {physicsDegree}");
            Console.WriteLine($"{"Math Degree:",-20} {mathDegree}");

            Console.ReadKey();

            degreeSum = programmingDegree + physicsDegree + mathDegree;
            averageDegree = degreeSum / 3;

            Console.WriteLine($"{"Degrees Sum:",-15} {degreeSum}");
            Console.WriteLine($"{"Average Degree:",-15} {averageDegree}");

            Console.ReadKey();
        }
    }
}
