﻿using System;

namespace Module_4
{
    internal class Task_1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter matrix rows quantity:");
            int rows = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter matrix columns quantity:");
            int cols = int.Parse(Console.ReadLine());

            int[,] matrix1 = new int[rows, cols];

            Random random = new Random();
            int maxRange = 1_000_000;
            int sum = 0;
            Console.WriteLine("First matrix is:");

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    matrix1[i, j] = random.Next(maxRange);
                    sum += matrix1[i, j];
                    Console.Write($"{matrix1[i, j]}\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine($"Sum of all matrix elements equals: {sum}");
            Console.ReadKey();

            int[,] matrix2 = new int[rows, cols];

            Console.WriteLine("Second matrix is:");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    matrix2[i, j] = random.Next(maxRange);
                    sum += matrix2[i, j];
                    Console.Write($"{matrix2[i, j]}\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Sum of both matrix is:");

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Console.Write($"{matrix1[i, j] + matrix2[i, j]}\t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
